provider "sops" {}

data "sops_file" "secrets" {
  source_file = "../_vars/secrets.enc.yml"
}

variable "aws_region" {
  type = string
}

locals {
  namespace           = var.namespace
  stage               = var.stage
  name                = "vpc-satellite"
  aws_region          = var.aws_region
  tags                = map("Project", data.sops_file.secrets.data["project_tag"])
  remote_state_bucket = "${local.namespace}-${local.stage}-terraform-state"
  us1_region          = data.sops_file.secrets.data["vpc_us1_region"]
  india_region        = data.sops_file.secrets.data["vpc_india_region"]
  vpc_ids = {
    main  = data.terraform_remote_state.vpc_main.outputs.vpc.vpc_id
    us1   = data.terraform_remote_state.vpc_satellites.outputs.vpc_us1.vpc_id
    india = data.terraform_remote_state.vpc_satellites.outputs.vpc_india.vpc_id
  }
}


