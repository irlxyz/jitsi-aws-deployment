provider "sops" {}

data "sops_file" "secrets" {
  source_file = "../_vars/secrets.enc.yml"
}
variable "aws_region" {
  type = string
}

locals {
  namespace                 = var.namespace
  stage                     = var.stage
  name                      = "letsencrypt-certs"
  aws_region                = var.aws_region
  tags                      = map("Project", data.sops_file.secrets.data["project_tag"])
  tls_private_key_algo      = "ECDSA"
  tls_private_key           = "P265"
  cloudflare_dns_api_token  = data.sops_file.secrets.data["cloudflare_dns_api_token"]
  cloudflare_zone_api_token = data.sops_file.secrets.data["cloudflare_zone_api_token"]
  common_name               = data.sops_file.secrets.data["jitsi_fqdn_eu"]
  force_renewal             = false
  dns_names                 = []
}

