terraform {
  required_version = ">= 0.12"

  backend "s3" {
    key = "jitsi-master/terraform.tfstate"
  }
  required_providers {
    sops = "~> 0.5"
  }
}

provider "aws" {}
provider "aws" {
  alias  = "ap-south-1"
  region = "ap-south-1"
}
provider "cloudflare" {
  version   = "~> 2.0"
  api_token = data.sops_file.secrets.data.cloudflare_dns_api_token
}

data "terraform_remote_state" "setup" {
  backend = "s3"

  config = {
    bucket = local.remote_state_bucket
    key    = "setup/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc_satellites" {
  backend = "s3"

  config = {
    bucket = local.remote_state_bucket
    key    = "vpc-satellites/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc_main" {
  backend = "s3"

  config = {
    bucket = local.remote_state_bucket
    key    = "vpc-main/terraform.tfstate"
  }
}

data "terraform_remote_state" "letsencrypt" {
  backend = "s3"

  config = {
    bucket = local.remote_state_bucket
    key    = "letsencrypt-certs/terraform.tfstate"
  }
}

