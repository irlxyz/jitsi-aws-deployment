output "vpc" {
  value = module.vpc
}

output "public_subnets" {
  value = module.public_subnets
}
