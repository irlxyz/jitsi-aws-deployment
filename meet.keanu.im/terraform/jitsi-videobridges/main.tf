terraform {
  required_version = ">= 0.12"

  backend "s3" {
    key = "jitsi-videobridges/terraform.tfstate"
  }
  required_providers {
    sops = "~> 0.5"
  }
}

data "terraform_remote_state" "setup" {
  backend = "s3"

  config = {
    bucket = local.remote_state_bucket
    key    = "setup/terraform.tfstate"
  }
}
data "terraform_remote_state" "vpc_main" {
  backend = "s3"

  config = {
    bucket = local.remote_state_bucket
    key    = "vpc-main/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc_satellites" {
  backend = "s3"

  config = {
    bucket = local.remote_state_bucket
    key    = "vpc-satellites/terraform.tfstate"
  }
}

data "terraform_remote_state" "letsencrypt" {
  backend = "s3"

  config = {
    bucket = local.remote_state_bucket
    key    = "letsencrypt-certs/terraform.tfstate"
  }
}

data "terraform_remote_state" "jitsi_master" {
  backend = "s3"

  config = {
    bucket = local.remote_state_bucket
    key    = "jitsi-master/terraform.tfstate"
  }
}
