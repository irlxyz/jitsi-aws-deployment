.PHONY: build all dev-debian-base
export AWS_SOURCE_REGION ?= $(AWS_REGION)
PACKER_BASE ?= ../../packer
ANSIBLE_BASE ?= ../../ansible
PACKER_ARGS ?=

deps:
	cd $(ANSIBLE_BASE) && $(MAKE) deps

jvb: deps clean packer.json .targets-jvb
	$(AWS_WRAP) "packer build $(PACKER_ARGS) -timestamp-ui -parallel=true -only='$(shell cat .targets-jvb)' $(PWD)/packer.json"

debian-base: deps clean packer.json .targets-debian-base
	$(AWS_WRAP) "packer build $(PACKER_ARGS) -timestamp-ui -parallel=true -only='$(shell cat .targets-debian-base)' $(PWD)/packer.json"

region/%: deps
	$(AWS_WRAP) "packer build $(PACKER_ARGS) -timestamp-ui -parallel=false -only='debian-base-$(@F),jvb-$(@F)' $(PWD)/packer.json"

jvb/%: deps
	$(AWS_WRAP) "packer build $(PACKER_ARGS) -timestamp-ui -parallel=true -only='jvb-$(@F)' $(PWD)/packer.json"

debian-base/%: deps
	$(AWS_WRAP) "packer build $(PACKER_ARGS) -timestamp-ui -parallel=true -only='debian-base-$(@F)' $(PWD)/packer.json"

packer.json:
	@gomplate -f $(PACKER_BASE)/templates/packer.json.tmpl -d config.yaml -o packer.json

.targets-jvb:
	@gomplate -f $(PACKER_BASE)/templates/targets-jvb.tmpl -d config.yaml -o .targets-jvb

.targets-debian-base:
	@gomplate -f $(PACKER_BASE)/templates/targets-debian-base.tmpl -d config.yaml -o .targets-debian-base

clean:
	@rm packer.json -f
	@rm .targets-* -f

dist-clean: clean
	@rm manifest.json
